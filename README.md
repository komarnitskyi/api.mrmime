# Mr.Mime API
> API for mr.mime app

### Install:
```bash
git clone https://<username>@bitbucket.org/komarnitskyi/api.mrmime.git
cd api.mrmime
npm i
```

### Start:
```bash
npm start
```

### Deploy:
```bash
npm run deploy
```
