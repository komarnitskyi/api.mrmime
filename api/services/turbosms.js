var Promise = require("bluebird");
// подключаем модуль soap клиента
var soap = require('soap');
// подключаем модуль для реализации soap cookies
var SoapCookie = require('soap-cookie');
// указываем данные пользователя
var user = {
	'login': 'ticapacasd',
	'password': 'asd123'
};

// задаем параметры отправки смс сообщения,
// заметьте что в поле sender указывается подпись которую
// Вы должны создать в панели администрирования сайта до того как ее
// использовать так как она должна пройти модерацию.
var messageDefault = {
	'sender': 'ASDTeam',
	//'destination': '+380988295917,+380503748110',
	'text': 'текст сообщения'
};
// задаем аддресс подключения к API
var url = 'http://turbosms.in.ua/api/wsdl.html';
/*client.GetCreditBalance(null, function (err, message) {
 console.log(message.GetCreditBalanceResult);

 });*/

// создаем подключение клиента
function Client(credentials) {
	var cl = new Promise(function (resolve, reject) {
		soap.createClient(url, function (err, client) {


			client.Auth(credentials, function (err, result) {
				resolve(client);
				client.setSecurity(new SoapCookie(client.lastResponseHeaders));
			});
		});
	});


	return {
		GetCreditBalance: ()=> {
			return new Promise((resolve, reject)=> {
				cl.then(client=> {
					client.GetCreditBalance(null, function (err, message) {
						resolve(message.GetCreditBalanceResult);

					})
				})
			});
		},

		sendSMS: (message)=> {
			return new Promise((resolve, reject)=> {
				cl.then(client=> {
					client.SendSMS(message, function (err, message) {
						resolve(message);

					})
				})
			});
		}


	};
};

var clients = [
  '+380988295917'
]


var turboClient = new Client(user);

module.exports.sendSMS = function(messageText){
  // console.log('Sent with params: ', {
  //   'sender': 'ASDTeam',
  //   'destination': clients.join(','),
  //   'text': messageText
  // });
  return turboClient.sendSMS({
    'sender': 'ASDTeam',
    'destination': clients.join(','),
    'text': messageText
  })
};
