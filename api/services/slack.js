// komarn  U49ASM0EP   im: D48MF5R24
// liuda U48N1F6FJ   im: D510Z3M8R
//https://hooks.slack.com/services/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
//
const komarnitksyiUri = 'https://hooks.slack.com/services/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';
const milaUri = 'https://hooks.slack.com/services/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';

var r = require('request');
const webHookUri = milaUri;

module.exports = {
  notifyAdmin: function (text) {
      r.post(webHookUri, {json:true, body: {text: text}});
  }
}
