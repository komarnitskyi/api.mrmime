/**
 * Items.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string',
      required: false
    },
    inBuyList: {
      type: 'boolean',
      default: false,
      required: true
    },
    private: {
      type: 'boolean',
      default: false,
      required: true
    },
    critical: {
      type: 'boolean',
      required: true
    },
    count: {
      type: 'string',
      enum: ['no', 'few', 'enough']
    },
    categories: {
      collection: 'categories',
      dominant: true,
      via: 'items'
    },
    markets: {
      collection: 'markets',
      dominant: true,
      via: 'items'
    }
  }
};
