/**
 * ReportController
 *
 * @description :: Server-side logic for managing reports
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 const json2xls = require('json2xls');
 const fs = require('fs');
 const Promise = require('bluebird');

module.exports = {
	getXls: getXls
};

function _getReport(reportData) {
	return new Promise(function(resolve, reject){

		Items.find(reportData.items.split(',')).exec(function(err, records) {

					reportData.items = records;
					reportData.items = records.length
												? records
												: [{name: 'Items was deleted from the collection'}];

					const creatingDate = new Date(reportData.createdAt);

					const report = {
						id: reportData.id,
						date: creatingDate.toDateString(),
						items: reportData.items.map(el=>el.name),
						price: reportData.price
					}

					resolve(report)
		})
	})
}

function getXls(req,res){
	const findParam = req.params.id !== 'all' ? {id: req.params.id} : {};
	//
	Report.find(findParam).exec(function (err, records) {

		Promise.all(records.filter(el=> el.items).map(el=> {
			if(el.items) return  _getReport(el);
		}))
			.then(function(...reports) {
				var xls = json2xls(reports[0]);

				const reportName = reports[0].length > 1
														? 'office-manager-report-total'
														: `office-manager-report-${reports[0][0].date}`
				fs.writeFileSync(`./assets/reports/${reportName}.xlsx`, xls, 'binary');

				res.ok({
					reportUrl: `/reports/${reportName}.xlsx`
				})
			})
			.catch(function(...errors) {
				res.badRequest(errors);
			})
});
}
