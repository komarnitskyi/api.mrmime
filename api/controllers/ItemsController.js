/**
 * ItemsController
 *
 * @description :: Server-side logic for managing items
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

 function setAsEnough(req, res) {
	sails.models.items.update({id: req.params.id}, {count: 'enough'}).exec((err, updated) => {
		res.ok(updated)
	})
 }

 function setAsOver(req, res) {
	sails.models.items.update({id: req.params.id}, {count: 'no'}).exec((err, updated) => {
    if(updated[0] && updated[0].critical) {
      // send sms notification
      const notificationContent = `Закінчилося: ${updated[0].name}`;

      slack.notifyAdmin(notificationContent);
    };
		res.ok(updated)
	})
 }

 function addToBuyList(req, res) {
	sails.models.items.update({id: req.params.id}, {inBuyList: true}).exec((err, updated) => {
		res.ok(updated)
	})
 }

 function removeFromBuyList(req, res) {
	sails.models.items.update({id: req.params.id}, {inBuyList: false}).exec((err, updated) => {
		res.ok(updated)
	})
 }

 function purchase(req, res) {
  const criteries = req.params.id.split(',');

  // sails.models.items.find(criteries).exec((err, updated) => {
	// 	res.ok(updated)
	// })

	sails.models.items.update(criteries, {inBuyList: false, count: 'enough'}).exec((err, updated) => {

    const report = {
      price: +req.params.price,
      items: req.params.id,
    }

    console.log('new report:', report)
    sails.models.report.create(report).exec( (err, createdReport) => {
      console.log('Error', err);
      console.log('result', createdReport);
      res.ok(createdReport)
    })

	})
 }

 function markAsBought(req, res) {

	sails.models.items.update({id: req.params.id}, {inBuyList: false, count: 'enough'}).exec((err, updated) => {
		res.ok(updated)
	})
 }

 function setAsFew(req, res) {
	sails.models.items.update({id: req.params.id}, {count: 'few'}).exec((err, updated) => {
    if(updated[0] && updated[0].critical) {
      // send sms notification
      slack.notifyAdmin(`Майже закінчилося: ${updated[0].name}`);

    };
		res.ok(updated)
	})
 }

 function notify(req, res) {
	sails.models.items.find({critical: true, count: 'no'}).exec((err, found) => {
    console.log(found)
    const itemsLabels = found.map(_=>_.name).join(',');

  //  console.log(itemsLabels)
    turbosms.sendSMS(`Закінчилося: ${itemsLabels}`)
    slack.notifyAdmin(`Закінчилося: ${itemsLabels}`);
		res.ok(found)
	})
 }

 // const tests = [
 //   {date: '10-01-2017', status: 'enough'},
 //   {date: '11-01-2017', status: 'enough'},
 //   {date: '12-01-2017', status: 'few'},
 //   {date: '13-01-2017', status: 'no'},
 //   {date: '15-01-2017', status: 'enough'},
 // ]
 //
 //
 // var r  = tests.reduce(function(acc,el){
 //
 //   return (acc[acc.length-1] && acc[acc.length-1].status === el.status) || el.status === 'few' ? [...acc] : [...acc, el];
 // }, []);
 // var res = [];
 //
 //
 // for (var i=0; i< r.length; i+=2) {
 //   res.push({from: r[i].date, to: r[i+1]? r[i+1].date: null})
 // }
 //
 // console.log(res)

module.exports = {
	setAsEnough: setAsEnough,
	setAsOver: setAsOver,
	setAsFew: setAsFew,
	markAsBought: markAsBought,
	purchase: purchase,
	addToBuyList: addToBuyList,
	removeFromBuyList: removeFromBuyList,
	notify: notify,

};
