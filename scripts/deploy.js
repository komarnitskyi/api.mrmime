const { host, username, password, port } = require('../ssh-config');
const path = require('path');
const colors = require('colors');
const uniq = require('lodash/uniq');
const Client = require('ssh2-sftp-client');

const { putDir, progress, done, error, finish } = require('./helpers');

const sftp = new Client();

const remotePath = '/var/public/api.mrmime'; // Remote path

console.log('\x1Bc');
console.log(colors.green('____________________________'));
console.log('');
console.log(' H O S T:         '.bgGreen.black, host.cyan);
console.log(' P O R T:         '.bgGreen.black, `${port}`.cyan);
console.log(' U S E R N A M E: '.bgGreen.black, username.cyan);
console.log(' P A S S W O R D: '.bgGreen.black, password.cyan);
console.log(colors.green('____________________________'));

sftp.connect({ host, port, username, password })
  .then(() => {
    progress(' Updating "app.js"... ');
    return sftp.delete(`${remotePath}/app.js`)
  })
  .catch(() => console.log(`${remotePath}/app.js - already deleted.`.yellow))
  .then(() => sftp.put(path.join(__dirname, '../app.js'), `${remotePath}/app.js`))
  // === ==== ==== ==== ==== ====
  .then(() => {
    progress(' Updating "package.json"... ');
    return sftp.delete(`${remotePath}/package.json`)
  })
  .catch(() => console.log(`${remotePath}/package.json - already deleted.`.yellow))
  .then(() => sftp.put(path.join(__dirname, '../package.json'), `${remotePath}/package.json`))
  // === ==== ==== ==== ==== ====
  .then(() => {
    progress(' Updating ".gitignore"... ');
    return sftp.delete(`${remotePath}/.gitignore`)
  })
  .catch(() => console.log(`${remotePath}/.gitignore - already deleted.`.yellow))
  .then(() => sftp.put(path.join(__dirname, '../.gitignore'), `${remotePath}/.gitignore`))
  // === ==== ==== ==== ==== ====
  .then(() => {
    progress(' Updating "Gruntfile.js"... ');
    return sftp.delete(`${remotePath}/Gruntfile.js`)
  })
  .catch(() => console.log(`${remotePath}/Gruntfile.js - already deleted.`.yellow))
  .then(() => sftp.put(path.join(__dirname, '../Gruntfile.js'), `${remotePath}/Gruntfile.js`))
  // === ==== ==== ==== ==== ====
  .then(() => {
    progress(' Updating "README.md"... ');
    return sftp.delete(`${remotePath}/README.md`)
  })
  .catch(() => console.log(`${remotePath}/README.md - already deleted.`.yellow))
  .then(() => sftp.put(path.join(__dirname, '../README.md'), `${remotePath}/README.md`))
  // === ==== ==== ==== ==== ====
  .then(() => {
    done();
    progress(' Updating "api" folder... ')
    return sftp.rmdir(`${remotePath}/api`, true);
  })
  .catch(() => console.log(' "api" folder already removed. '))
  .then(() => sftp.mkdir(`${remotePath}/api`))
  .then(() => putDir(path.join(__dirname, '../api'), `${remotePath}/api`, sftp))
  // === ==== ==== ==== ==== ====
  .then(() => {
    done();
    progress(' Updating "assets" folder... ');
    return sftp.rmdir(`${remotePath}/assets`, true);
  })
  .catch(() => console.log(' "assets" folder already removed '))
  .then(() => {
    return sftp.mkdir(`${remotePath}/assets`);
  })
  .then(() => putDir(path.join(__dirname, '../assets'), `${remotePath}/assets`, sftp))
  // === ==== ==== ==== ==== ====
  .then(() => {
    done();
    progress(' Updating "config" folder... ');
    return sftp.rmdir(`${remotePath}/config`, true);
  })
  .catch(() => console.log(' "config" folder already removed '))
  .then(() => sftp.mkdir(`${remotePath}/config`))
  .then(() => putDir(path.join(__dirname, '../config'), `${remotePath}/config`, sftp))
  // === ==== ==== ==== ==== ====
  .then(() => {
    done();
    progress(' Updating "scripts" folder... ');
    return sftp.rmdir(`${remotePath}/scripts`, true);
  })
  .catch(() => console.log(' "scripts" folder already removed '))
  .then(() => sftp.mkdir(`${remotePath}/scripts`))
  .then(() => putDir(path.join(__dirname, '../scripts'), `${remotePath}/scripts`, sftp))
  // === ==== ==== ==== ==== ====
  .then(() => {
    done();
    progress(' Updating "tasks" folder... ');
    return sftp.rmdir(`${remotePath}/tasks`, true);
  })
  .catch(() => console.log(' "tasks" folder already removed '))
  .then(() => sftp.mkdir(`${remotePath}/tasks`))
  .then(() => putDir(path.join(__dirname, '../tasks'), `${remotePath}/tasks`, sftp))
  // === ==== ==== ==== ==== ====
  .then(() => {
    done();
    progress(' Updating "views" folder... ');
    return sftp.rmdir(`${remotePath}/views`, true);
  })
  .catch(() => console.log(' "views" folder already removed '))
  .then(() => sftp.mkdir(`${remotePath}/views`))
  .then(() => putDir(path.join(__dirname, '../views'), `${remotePath}/views`, sftp))
  // === ==== ==== ==== ==== ====
  .then(() => {
    finish();
    sftp.end();
  })
  .catch((err) => {
    error();
    sftp.end();
    console.log('Error: ', err);
  });
